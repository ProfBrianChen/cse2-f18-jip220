// Jamaica Perrier-Morris
//Lab04
//CSE002
//September 21 2018
//My program will be able to randomly select a card from a deck of card. 
//importing math.random
import java.util.Random;
//defining class
public class CardGenerator {
//adding main method
public static void main(String [] args) { 
//naming the number of the card pulled from deck
Random rand = new Random(); 
int value = rand.nextInt(52);
int numCardPulled = (int) ((Math.random() * 52) + 1);
//declaring the "identity" of the card aka what suit it is
String cardSuit = " ";
//choosing the suit of the card randomly using a switch statment
switch ((int) (numCardPulled/13)) {
    
 case (0):
     cardSuit = "Clubs"; 
  break;
    
 case (1):
      cardSuit = "Hearts";
  break;
    
 case (2):
      cardSuit = "Spades";
  break;
    
 case (3):
      cardSuit = "Diamonds";
  break;
    
  }
  //Assigning the suit of that card to a number 1-13 using a switch statement and then printing the answer to the screen.
  switch ((int) (numCardPulled % 13)) {
      
    case (0):
    System.out.println("You picked the King of " + cardSuit);
      break;
      
    case (1): 
    System.out.println("You picked the Jack of " + cardSuit);
      break;
      
    case (2):
    System.out.println("You picked the Ace of " + cardSuit);
      break;
      
    case (3):
    System.out.println("You picked the Queen of " + cardSuit);
      break;
    default:
        System.out.println("You picked the " + numCardPulled % 13 + " of " + cardSuit + ".");
  } 
} //end of class
} //end of main method
