/// Jamaica Perrier-Morris
/// lab06
/// 10/17/18
///This program is going to display a pyramid of numbers according to the number of rows inputted by the user. 
// This is PatternD
import java.util.Scanner;
 
public class DisplayPyramidD{
    public static void main(String [] args) {
      Scanner myScan = new Scanner(System.in);
      System.out.println("Enter the number for the pyramid");
      int val = myScan.nextInt();
      for (int i = val; i <= 1; i++) 
      { 
        for (int j = val; j <= 1 - i; j++) { 
          System.out.print("  "); 
        } 
        for (int k = 1; k <= i; k++) { 
          System.out.print(k + " "); 
        } 
        System.out.println(); 
      } 
    }
}