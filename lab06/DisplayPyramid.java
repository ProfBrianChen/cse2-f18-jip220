/// Jamaica Perrier-Morris
/// lab06
/// 10/17/18
///This program is going to display a pyramid of numbers according to the number of rows inputted by the user. 
// This is PatternA
import java.util.Scanner;
 
public class DisplayPyramid{
    public static void main(String [] args) {
      Scanner myScan = new Scanner(System.in);
      System.out.println("Enter the number for the pyramid");
      int val = myScan.nextInt();
      for (int i = 1; i <= val; i++) 
      { 
        for (int j = 1; j <= val - i; j++) { 
          System.out.print(""); 
        } 
        for (int k = 1; k <= i; k++) { 
          System.out.print(k + "  "); 
        } 
        System.out.println(); 
      } 
    }
}