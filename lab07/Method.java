///Jamaica Perrier-Morris
///CSE 002
///Lab 07
///This program is going to randomly create a sentence and then ranodmly create a paragraph. 

//import random class
import java.util.Random;
//import scanner csass
import java.util.Scanner;

public class Method{
    //insert main method
    public static void main(String[] args){
        //inser scanner class
        Scanner myScan = new Scanner(System.in);
        int x = 0;
        //ask the user if they want to generate a sentence
        System.out.print("Would you like to create a sentence? Press 'y' for yes and 'n' for no: ");
        while (x == 0){
          //create a while loop to genetate a sentence, and make sure they use a string
            while(!myScan.hasNext()){
              
                System.out.print("Cannot continue. Please press either 'y' or 'n': ");
                myScan.next();
            }
            String y = myScan.nextLine();
            y = y.toLowerCase();
            //create a switch
            switch(y){ 
                case "y":
            String k = Adjectives();
            String l = Subject();
            String g = Verb();
            String o = Obj();
            System.out.println("The "+k+ " "+l+" "+g+" the "+o+". ");
                //Ask the user if theyd like to generate a second sentence
            System.out.print("Would you like to create another sentence? push 'y' for yes and 'n' for no: ");
                //create a loop
                while(!myScan.hasNext()){
                System.out.println("Cannot continue. Please press either 'y' or 'n': ");
                myScan.next();
            }
            break;
                case "n":
                    x = 2; 
                    break;
            }
        
        }
    System.out.println(Final());
}
    //addi adjectives method
    public static String Adjectives(){
        //declare randomgenerator
        Random randomGenerator= new Random();
        int randomInt = randomGenerator.nextInt(5);
        String a = "";
        //enter switch
        switch(randomInt){
            case 0:
                a = "quick";
                break;
            case 1:
                a = "brown";
                break;
            case 2: 
                a = "beautiful";
                break;
            case 3:
                a = "proud";
                break;
            case 4:
                a = "frustrating";
                break;
       
        }
        //return word 
        return a;
    }//end of method
  
    //add subject method
    public static String Subject(){
        Random randomGenerator= new Random();
        int randomInt = randomGenerator.nextInt(5);
        String b = "";
        switch(randomInt){
            case 0:
                b = "fox";
                break;
            case 1:
                b = "dog";
                break;
            case 2:
                b = "girl";
                break;
            case 3:
                b = "boy";
                break;
            case 4:
                b = "puppy";
                break;
        
        }
        //return word
        return b;
    }//end of class
    //adding verb method
    public static String Verb(){
        Random randomGenerator= new Random();
        int randomInt = randomGenerator.nextInt(5);
        String c = "";
        switch(randomInt){
            case 0:
                c = "passed";
                break;
            case 1:
                c = "chased";
                break;
            case 2:
                c = "hugged";
                break;
            case 3: 
                c = "played";
                break;
            case 4:
                c = "followed";
                break;
        }
        //return word
        return c;
    }//end of method
    //adding object method
    public static String Obj(){
        Random randomGenerator= new Random();
        int randomInt = randomGenerator.nextInt(5);
        String d = "";
        switch(randomInt){
            case 0: 
                d = "teacher";
                break;
            case 1:
                d = "car";
                break;
            case 2:
                d = "bike";
                break;
            case 3:
                d = "cat";
                break;
            case 4:
                d = "prince";
                break;
            
        }
        //return word 
        return d;
    }//end of method
    //adding thesis method
    public static String Thesis(){
        //calling adjective method
        String k = Adjectives();
        //calling sbject method
        String l = Subject();
        //calling verb method
        String g = Verb();
        //calling object method
        String o = Obj();
        System.out.print("The "+k+ " "+l+" "+g+" the "+o+". ");
      //return the subject
      return l;
    }//end of method
    //adding body method
    public static String body(){
        Random randomGenerator= new Random();
        int randomInt = randomGenerator.nextInt(5);
        //calling adjective method
        String k = Adjectives();
        //calling verb method
        String g = Verb();
        //calling object method.
        String o = Obj();
        //creating empty string for switch statement
        String sub;
        switch(randomInt%2){
            case 0:
                sub = "It";
                break;
            default:
                sub = "The "+Thesis();
                break;
        }
        //generate action sentence
        String bod = sub+" "+g+" the "+k+" "+o+". ";
      //return action sentence
      return bod;
    }//end of method
    //adding method conclusion
    public static String conclusion(){
         Random randomGenerator= new Random();
        int randomInt = randomGenerator.nextInt(5);
        //calling adjective method
        String k = Adjectives();
        //calling verb method
        String g = Verb();
        //calling object method
        String o = Obj();
        //empty string for switch statement
        String subj;
        switch(randomInt%2){
            case 0:
                subj = "It";
                break;
            default:
                subj = "The "+Thesis();
                break;
        }
        String con = subj+" "+g+" the "+o+". ";
        //return the sentence
        return con;
        
    }//end of method
    //adding method final
    public static String Final(){
        String con="";
        Random randomGenerator= new Random();
        int randomInt = randomGenerator.nextInt(10);
        for(int i=0; i<randomInt; i++){
        String body = body();
        System.out.print(body);
        }
        con = conclusion();
        return con;
    //end of method
    }
    
//end of class
}