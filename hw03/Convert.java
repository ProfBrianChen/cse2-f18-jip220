// Jamaica Perrier-Morris
// CSE2
// September 16th 2018
// Homework 3
//This program will calculate the number or acres effected by a hurricane and how much rain fell.
//This program will convert the amount of rain  into cubic miles. 

//import scanner class
import java.util.Scanner;

//defining class
public class Convert{ 

  //adding main method
public static void main(String args []) {
 Scanner myScanner = new Scanner(System.in);
System.out.print("Enter the affected area in acres: ");
double area = myScanner.nextDouble();
System.out.print("Enter the rainfall in inches: ");
double rainfall = myScanner.nextDouble(); 
double val = (((area * rainfall) * 27154.2857) * (double)(9.08169*((double) Math.pow (10, -13))));
System.out.println(+val+ " cubic miles");
}
                   //end of main method
                   }//end of class;
                   
           