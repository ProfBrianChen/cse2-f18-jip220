//Jamaica Perrier-Morris
//CSE02
//September 17 2018
//This program is going to be able to compute the volume inside of a pyramid.

//input Scanner class
import java.util.Scanner;
//defining class
public class Pyramid{
  //adding the main method
  public static void main(String [] args){
    
  Scanner myScanner = new Scanner(System.in);
  System.out.println("The square side of the pyramid is: (input Length in the form of xx) "); 
  double pyramidLength = myScanner.nextDouble();
  System.out.println("The height of the pyramid is: ");
  double pyramidHeight = myScanner.nextDouble ();
  System.out.println("The width of the pyramid is: ");
  double pyramidWidth = myScanner.nextDouble (); 
  int volume = (int) (pyramidLength * pyramidHeight * pyramidWidth)/ 3;
  System.out.println("The volume inside of the pyramid is: "+ volume);
}//end of main method
}//end of class 
 