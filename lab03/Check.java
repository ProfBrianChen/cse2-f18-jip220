// Jamaica Perrier-Morris
// CSE 002
// September 14th 2018
// Lab03 
// This program will use find how much each person in a group needs to pay for a split check. It will find the origional cost of a check, the percentage of tip they want to pay, and the number of ways the check will be split.

//importing the canner class
import java.util.Scanner;

//defining class
public class Check{
  //adding main method 
  public static void main(String [] args){ 

Scanner myScanner = new Scanner(System.in);
System.out.print("Enter the origional cost of the check in the form xx.xx: "); 
double checkCost = myScanner.nextDouble ();
System.out.print("Enter the number of people who went to dinner: ");
double tipPercent = myScanner.nextDouble() ;
tipPercent  /= 100; //We want to concert the percentage into a decimal value
System.out.print("Enter the number of people who went to dinner");
int numPeople = myScanner.nextInt ();
double totalCost;
double costPerPerson;
int dollars, //whole dollar amount of cost 
 dimes, pennies; //for storing digits 
                 // to the right of the decimal point
                 //for the cost$
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping the decimal fraction
 dollars=(int)costPerPerson;
//get dimes amount, e.g.,
// (int) (6.73 * 10) % 10 -> 67 % 10 -> 7 
// where the % (mod) operator return the remainder
// after the division: 503%100 -> 83, 27v5 -> 27v5
dimes = (int) (costPerPerson * 10) % 10;
pennies = (int) (costPerPerson * 100) % 10;
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); 
                 
  }//end of main method
}//end of class
                 